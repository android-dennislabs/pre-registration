# pre-registration  

#### Library size : 48Kb(lib) + 362.5Kb(retrofit2:converter-gson, okhttp3:logging-interceptor) = 410.5KB(Total)
  
## Setup Project

Add this in your proguard

-keep class com.preregistration.*.* { *; }
-keepclassmembers class com.preregistration.*.* { *; }
-dontwarn com.preregistration.*.*
 
 

Add this to your project build.gradle
``` gradle
allprojects {
    repositories {
        maven {
            url "https://jitpack.io"
        }
    }

    ext {
        appcompat = '1.2.0'
        material = '1.3.0'
        retrofit_version = '2.3.0'
        retrofit_okhttp_version = '3.10.0'
    }
}
```

Add this to your project build.gradle

#### Dependency
[![](https://jitpack.io/v/org.bitbucket.android-dennislabs/pre-registration.svg)](https://jitpack.io/#org.bitbucket.android-dennislabs/pre-registration)
```gradle
dependencies {
    implementation 'org.bitbucket.android-dennislabs:pre-registration:1.0'
}
```
Needed support libs
```gradle
dependencies {
    implementation "androidx.appcompat:appcompat:$rootProject.ext.appcompat"
    implementation "com.google.android.material:material:$rootProject.ext.material"
}
```


### Usage methods
```java
public class MainActivity extends AppCompatActivity {

    private String sampleJson = "{\"id\":52,\"title\":\"Bihar Board\",\"sub_title\":\"Whiteboard->Result->Board Exam->Bihar Board->10 Class\",\"requestApi\":\"https://www.fastresult.in/api/v3/api-name\",\"streams\":[{\"id\":1,\"title\":\"PCM\"},{\"id\":2,\"title\":\"PCMB\"},{\"id\":3,\"title\":\"Arts\"},{\"id\":4,\"title\":\"Commerce\"}],\"popup\":{\"title\":\"Thank You!\",\"description\":\"You will get your result soon\",\"submit_btn_text\":\"Continue\"},\"board_result_field\":[{\"id\":1,\"title\":\"Roll Number\",\"input_type\":2},{\"id\":2,\"title\":\"Roll Code\",\"input_type\":2}]}";
    private boolean isOpenActivityByJson = true;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PreRegistration.getInstance()
                .setDebugModeEnabled(BuildConfig.DEBUG)
                .setAppVersion(BuildConfig.VERSION_NAME);
    }

    public void onOpenPreRegistration(View view) {
        if(isOpenActivityByJson) {
            openPreRegistrationByJson();
        }else {
            openPreRegistrationByModel();
        }
    }

    private void openPreRegistrationByJson() {
        PreRegistration.getInstance().openRegistrationActivity(this, sampleJson);
    }

    private void openPreRegistrationByModel() {
        PreRegistration.getInstance().openRegistrationActivity(this, getCategoryProperty());
    }

    public void onClearPreferences(View view) {
        PRPreferences.setRegistrationCompleted(this, false);
    }

    private PreRegistrationModel getCategoryProperty() {
        PreRegistrationModel item = new PreRegistrationModel();
        item.setTitle("UP Board Result");
        item.setSubTitle("Note: To get UP Board Result update on WhatsApp and Gmail");
        item.setRequestApi("https://www.fastresult.in/api/v3/submit-form");
        item.setStreams(getSteams());
        item.setPopup(getPopup());
        item.setInputFields(getInputField());
        return item;
    }

    private List<DynamicInputModel> getInputField() {
        List<DynamicInputModel> fieldList = new ArrayList<>();
        DynamicInputModel item;

        item = new DynamicInputModel();
        item.setId(1);
        item.setTitle("Roll Number");
        item.setInputType(FieldInputType.number);
        fieldList.add(item);

        item = new DynamicInputModel();
        item.setId(2);
        item.setTitle("Roll Code");
        item.setInputType(FieldInputType.number);
        fieldList.add(item);

        return fieldList;
    }

    private List<StreamEntity> getSteams() {
        List<StreamEntity> steamsList = new ArrayList<>();
        StreamEntity item;

        item = new StreamEntity();
        item.setId(1);
        item.setTitle("PCM");
        steamsList.add(item);

        item = new StreamEntity();
        item.setId(2);
        item.setTitle("PCMB");
        steamsList.add(item);

        item = new StreamEntity();
        item.setId(3);
        item.setTitle("Arts");
        steamsList.add(item);

        item = new StreamEntity();
        item.setId(4);
        item.setTitle("Commerce");
        steamsList.add(item);

        return steamsList;
    }

    private PopupEntity getPopup() {
        PopupEntity popupEntity = new PopupEntity();
        popupEntity.setTitle("Thank You!");
        popupEntity.setDescription("You will get your result soon");
        popupEntity.setSubmitBtnText("Continue");
        return popupEntity;
    }
}
```