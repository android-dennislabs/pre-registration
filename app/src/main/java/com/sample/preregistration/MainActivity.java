package com.sample.preregistration;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.preregistration.PreRegistration;
import com.preregistration.interfaces.FieldInputType;
import com.preregistration.model.DynamicInputModel;
import com.preregistration.model.PreRegistrationModel;
import com.preregistration.model.entity.PopupEntity;
import com.preregistration.model.entity.StreamEntity;
import com.preregistration.util.PRPreferences;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private String sampleJson = "{\"id\":52,\"title\":\"Bihar Board\",\"sub_title\":\"Whiteboard->Result->Board Exam->Bihar Board->10 Class\",\"requestApi\":\"https://www.fastresult.in:8081/api/v9/save-pre-register-forum-data\",\"streams\":[],\"popup\":{\"title\":\"Thank You!\",\"description\":\"You will get your result soon\",\"submit_btn_text\":\"Continue\"},\"board_result_field\":[{\"id\":1,\"title\":\"Roll Number\",\"input_type\":2}]}";
    private boolean isOpenActivityByJson = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PreRegistration.getInstance()
                .setDebugModeEnabled(BuildConfig.DEBUG)
                .setAppVersion(BuildConfig.VERSION_NAME);
    }

    public void onOpenPreRegistration(View view) {
        if(isOpenActivityByJson) {
            openPreRegistrationByJson();
        }else {
            openPreRegistrationByModel();
        }
    }

    private void openPreRegistrationByJson() {
        PreRegistration.getInstance().openRegistrationActivity(this, sampleJson);
    }

    private void openPreRegistrationByModel() {
        PreRegistration.getInstance().openRegistrationActivity(this, getCategoryProperty());
    }

    public void onClearPreferences(View view) {
        PRPreferences.setRegistrationCompleted(this, false);
    }

    private PreRegistrationModel getCategoryProperty() {
        PreRegistrationModel item = new PreRegistrationModel();
        item.setTitle("UP Board Result");
        item.setSubTitle("Note: To get UP Board Result update on WhatsApp and Gmail");
        item.setRequestApi("https://www.fastresult.in/api/v3/submit-form");
        item.setStreams(getSteams());
        item.setPopup(getPopup());
        item.setInputFields(getInputField());
        return item;
    }

    private List<DynamicInputModel> getInputField() {
        List<DynamicInputModel> fieldList = new ArrayList<>();
        DynamicInputModel item;

        item = new DynamicInputModel();
        item.setId(1);
        item.setTitle("Roll Number");
        item.setInputType(FieldInputType.number);
        fieldList.add(item);

        item = new DynamicInputModel();
        item.setId(2);
        item.setTitle("Roll Code");
        item.setInputType(FieldInputType.number);
        fieldList.add(item);

        return fieldList;
    }

    private List<StreamEntity> getSteams() {
        List<StreamEntity> steamsList = new ArrayList<>();
        StreamEntity item;

        item = new StreamEntity();
        item.setId(1);
        item.setTitle("PCM");
        steamsList.add(item);

        item = new StreamEntity();
        item.setId(2);
        item.setTitle("PCMB");
        steamsList.add(item);

        item = new StreamEntity();
        item.setId(3);
        item.setTitle("Arts");
        steamsList.add(item);

        item = new StreamEntity();
        item.setId(4);
        item.setTitle("Commerce");
        steamsList.add(item);

        return steamsList;
    }

    private PopupEntity getPopup() {
        PopupEntity popupEntity = new PopupEntity();
        popupEntity.setTitle("Thank You!");
        popupEntity.setDescription("You will get your result soon");
        popupEntity.setSubmitBtnText("Continue");
        return popupEntity;
    }
}