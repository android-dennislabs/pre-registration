package com.preregistration;

import android.content.Context;
import android.content.Intent;

import com.preregistration.activity.PreRegistrationActivity;
import com.preregistration.model.PreRegistrationModel;
import com.preregistration.util.GsonParser;
import com.preregistration.util.PRConstant;
import com.preregistration.util.PRPreferences;
import com.preregistration.util.PRUtility;

public class PreRegistration {

    public static final int LIBRARY_VERSION = 1;

    private static volatile PreRegistration sSoleInstance;
    public boolean isDebugModeEnabled = false;
    public String appVersion;

    private PreRegistration() {

    }

    public static PreRegistration getInstance() {
        if (sSoleInstance == null) {
            synchronized (PreRegistration.class) {
                if (sSoleInstance == null) sSoleInstance = new PreRegistration();
            }
        }
        return sSoleInstance;
    }

    public boolean isDebugModeEnabled() {
        return isDebugModeEnabled;
    }

    public PreRegistration setDebugModeEnabled(boolean debugModeEnabled) {
        isDebugModeEnabled = debugModeEnabled;
        return this;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public PreRegistration setAppVersion(String appVersion) {
        this.appVersion = appVersion;
        return this;
    }

    public boolean isRegistrationCompleted(Context context) {
        return PRPreferences.isRegistrationCompleted(context);
    }

    public void openRegistrationActivity(Context context, String json) {
        PreRegistrationModel model = GsonParser.getGson().fromJson(json, PreRegistrationModel.class);
        if (model != null) {
            openRegistrationActivity(context, model);
        } else {
            PRUtility.showToastCentre(context, PRConstant.Error.INVALID_FORM_DATA);
        }
    }

    public void openRegistrationActivity(Context context, PreRegistrationModel categoryProperty) {
        if (!isRegistrationCompleted(context)) {
            context.startActivity(new Intent(context, PreRegistrationActivity.class)
                    .putExtra(PRConstant.CATEGORY_PROPERTY, categoryProperty));
        } else {
            PRUtility.showToastCentre(context, context.getString(R.string.error_form_already_submitted));
        }
    }
}
