package com.preregistration.activity;


import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.reflect.TypeToken;
import com.preregistration.R;
import com.preregistration.adapter.PREmailSuggestionAdapter;
import com.preregistration.adapter.PRFieldInputAdapter;
import com.preregistration.adapter.PRSpinnerAdapter;
import com.preregistration.interfaces.PRResponse;
import com.preregistration.model.DynamicInputModel;
import com.preregistration.model.PreRegistrationModel;
import com.preregistration.model.entity.StreamEntity;
import com.preregistration.network.PRNetworkManager;
import com.preregistration.util.FieldValidation;
import com.preregistration.util.GsonParser;
import com.preregistration.util.PRAlertUtil;
import com.preregistration.util.PRConstant;
import com.preregistration.util.PRPreferences;
import com.preregistration.util.PRUtility;
import com.preregistration.util.ProgressButton;

import java.util.ArrayList;
import java.util.List;

public class PreRegistrationActivity extends AppCompatActivity implements TextWatcher, View.OnKeyListener{

    private String title;
    private TextView tvSubTitle;
    private EditText etName, etMobile;
    private PreRegistrationModel property;
    private PRNetworkManager networkManager;
    private PRFieldInputAdapter adapter;
    private final List<DynamicInputModel> mList = new ArrayList<>();
    private ProgressButton btnAction;
    private AutoCompleteTextView etEmail;
    private Spinner spinnerInput;
    private StreamEntity mSelectedSteam;
    private View spinnerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pre_registration_activity);
        initArguments();
        initUi();

        loadData();
    }

    private void loadData() {
        tvSubTitle.setText(property.getSubTitle());
        if(!TextUtils.isEmpty(property.getRegistrationBtnTitle())){
            btnAction.setText(property.getRegistrationBtnTitle());
        }
        loadList(property.getInputFields());
        loadSpinner();
    }

    private void initUi() {
        tvSubTitle = findViewById(R.id.tv_note);
        etName = findViewById(R.id.et_user_name);
        etMobile = findViewById(R.id.et_user_mobile);
        etEmail = findViewById(R.id.et_user_email);

        etMobile.addTextChangedListener(this);
        etEmail.addTextChangedListener(this);
        etEmail.setOnKeyListener(this);

        RecyclerView rvList = findViewById(R.id.recycler_view);
        rvList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new PRFieldInputAdapter(mList);
        rvList.setAdapter(adapter);

        btnAction = ProgressButton.newInstance(this)
                .setText("Register")
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!FieldValidation.isValidField(v.getContext(), etMobile, etEmail)){
                            return;
                        } else if(etMobile.getText().length() > 0 && !FieldValidation.isMobileNumber(v.getContext(), etMobile, true)){
                            return;
                        } else if(etEmail.getText().length() > 0 && !FieldValidation.isEmail(v.getContext(), etEmail, true)){
                            return;
                        }
                        PRUtility.hideKeyboard(PreRegistrationActivity.this);
                        submitRequest();
                    }
                });
        setEnableButton(false);

        List<String> suggestions = GsonParser.fromJson(PRConstant.EMAIL_SUGGESTIONS, new TypeToken<List<String>>() {
        });
        if(suggestions != null) {
            PREmailSuggestionAdapter adapter = new PREmailSuggestionAdapter(this, android.R.layout.simple_list_item_1, suggestions);
            etEmail.setAdapter(adapter);
        }

        spinnerInput = findViewById(R.id.spinner_input);
        spinnerLayout = findViewById(R.id.spinner_input_layout);
        spinnerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spinnerInput != null) {
                    spinnerInput.performClick();
                }
            }
        });
    }

    private void setEnableButton(boolean isEnable) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnAction.setBackgroundTint(isEnable ? R.color.pre_color_register_button : R.color.pre_color_register_button_disable);
        }
    }

    private void loadSpinner() {
        if(property.getStreams() != null && property.getStreams().size() > 0) {
            PRSpinnerAdapter adapter = new PRSpinnerAdapter(this, getStreamList(property.getStreams()));
            spinnerInput.setAdapter(adapter);
            spinnerInput.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mSelectedSteam = (StreamEntity) parent.getItemAtPosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            spinnerLayout.setVisibility(View.VISIBLE);
        }else {
            spinnerLayout.setVisibility(View.GONE);
        }
    }

    private List<StreamEntity> getStreamList(List<StreamEntity> streams) {
        List<StreamEntity> mList = new ArrayList<>(streams);
        mList.add(0, new StreamEntity(0, "Select Stream"));
        return mList;
    }

    private void initArguments() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getSerializable(PRConstant.CATEGORY_PROPERTY) instanceof PreRegistrationModel) {
            property = (PreRegistrationModel) bundle.getSerializable(PRConstant.CATEGORY_PROPERTY);
            title = property.getTitle();
            networkManager = new PRNetworkManager(this, property.getRequestApi());
            setUpToolBar();
        }else {
            PRUtility.showPropertyError(this);
        }
    }

    private void loadList(List<DynamicInputModel> list) {
        mList.clear();
        if (list != null && list.size() > 0) {
            mList.addAll(list);
        }
        adapter.notifyDataSetChanged();
    }

    private void submitRequest() {
        String name = etName.getText().toString();
        String mobile = etMobile.getText().toString();
        String email = etEmail.getText().toString();

        if (btnAction != null) {
            btnAction.startProgress(new ProgressButton.Listener() {
                @Override
                public void onAnimationCompleted() {
                    int streamId = 0;
                    if(mSelectedSteam != null){
                        streamId = mSelectedSteam.getId();
                    }
                    if (networkManager != null && property != null) {
                        networkManager.submitRegistration(property.getId(), mList, name, mobile, email, streamId, new PRResponse.Callback<Boolean>() {
                            @Override
                            public void onSuccess(Boolean response) {
                                if (response) {
                                    PRPreferences.setRegistrationCompleted(PreRegistrationActivity.this, true);
                                    btnAction.revertSuccessProgress(new ProgressButton.Listener() {
                                        @Override
                                        public void onAnimationCompleted() {
                                            PRAlertUtil.showSuccessDialog(PreRegistrationActivity.this, property.getPopup());
                                        }
                                    });
                                } else {
                                    btnAction.revertProgress();
                                    PRUtility.showToastCentre(PreRegistrationActivity.this, PRConstant.Error.MSG_ERROR);
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    private void setUpToolBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            if (!TextUtils.isEmpty(title)) {
                actionBar.setTitle(title);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if( id == android.R.id.home ){
            onBackPressed();
            return true ;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        try {
            setEnableButton(etMobile.getText().length() > 0 || etEmail.getText().length() > 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
            PRUtility.hideKeyboard(this);
            return true;
        }
        return false;
    }
}
