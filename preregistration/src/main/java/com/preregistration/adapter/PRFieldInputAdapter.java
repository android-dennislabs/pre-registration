package com.preregistration.adapter;

import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;
import com.preregistration.R;
import com.preregistration.interfaces.FieldInputType;
import com.preregistration.model.DynamicInputModel;

import java.util.List;

public class PRFieldInputAdapter extends RecyclerView.Adapter<PRFieldInputAdapter.EditTextViewHolder> {
    private final List<DynamicInputModel> mList;

    protected RecyclerView mRecyclerView;

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        mRecyclerView = recyclerView;
    }

    public PRFieldInputAdapter(List<DynamicInputModel> mList) {
        this.mList = mList;
    }

    @NonNull
    @Override
    public EditTextViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new EditTextViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pre_slot_edit_text, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull EditTextViewHolder holder, int position) {
        DynamicInputModel item = mList.get(position);
        holder.etInputLayout.setHint(item.getTitle());
        holder.etInputText.setInputType(getInputType(item.getInputType()));
        holder.etInputText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                item.setInputData(s.toString());
            }
        });
    }

    private int getInputType(@FieldInputType int inputType) {
        if(inputType == FieldInputType.number){
            return InputType.TYPE_CLASS_NUMBER;
        }else {
            return InputType.TYPE_CLASS_TEXT;
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class EditTextViewHolder extends RecyclerView.ViewHolder {
        private final AutoCompleteTextView etInputText;
        private final TextInputLayout etInputLayout;

        public EditTextViewHolder(View view) {
            super(view);
            etInputLayout = view.findViewById(R.id.et_input_layout);
            etInputText = view.findViewById(R.id.et_input_text);
            etInputText.setThreshold(1);
            etInputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        try {
                            if(mRecyclerView != null && mRecyclerView.getLayoutManager() != null && getAdapterPosition() >= 0) {
                                View child = mRecyclerView.getLayoutManager().getChildAt(getAdapterPosition());
                                if (child != null) {
                                    child.setFocusable(true);
                                    child.setFocusableInTouchMode(true);
                                    child.requestFocus();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return true;
                    }
                    return false;
                }
            });
        }
    }
}