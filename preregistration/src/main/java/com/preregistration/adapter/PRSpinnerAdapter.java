package com.preregistration.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.preregistration.model.entity.StreamEntity;

import java.util.List;

public class PRSpinnerAdapter extends ArrayAdapter<StreamEntity> {

    public PRSpinnerAdapter(Context context, List<StreamEntity> algorithmList) {
        super(context, 0, algorithmList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable
            View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable
            View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }
        TextView textViewName = convertView.findViewById(android.R.id.text1);
        StreamEntity currentItem = getItem(position);
        if (currentItem != null) {
            textViewName.setText(currentItem.getTitle());
        }
        return convertView;
    }
}