package com.preregistration.interfaces;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({FieldInputType.text, FieldInputType.number})
@Retention(RetentionPolicy.SOURCE)
public @interface FieldInputType {
    int text = 1;
    int number = 2;
}
