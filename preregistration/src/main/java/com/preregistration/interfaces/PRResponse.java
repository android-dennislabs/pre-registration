package com.preregistration.interfaces;

public interface PRResponse {

    interface Callback<T> {
        void onSuccess(T response);

        default void onFailure(Exception e){}
    }
}