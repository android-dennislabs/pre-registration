package com.preregistration.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.preregistration.util.GsonParser;

import java.io.Serializable;

public class DynamicInputModel implements Serializable {

    @Expose
    @SerializedName(value="id")
    private int id;
    @Expose
    @SerializedName(value="title")
    private String title;
    @Expose
    @SerializedName(value="input_type")
    private int inputType;
    @Expose
    @SerializedName(value="input_data")
    private String inputData;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getInputType() {
        return inputType;
    }

    public void setInputType(int inputType) {
        this.inputType = inputType;
    }

    public String getInputData() {
        return inputData;
    }

    public void setInputData(String inputData) {
        this.inputData = inputData;
    }

    public String toJson(boolean excludeFieldsWithoutExposeAnnotation) {
        if(excludeFieldsWithoutExposeAnnotation){
            return GsonParser.toJson(this, new TypeToken<DynamicInputModel>() {});
        }else {
            return GsonParser.getGson().toJson(this, DynamicInputModel.class);
        }
    }
}
