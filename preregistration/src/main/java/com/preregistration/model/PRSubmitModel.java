package com.preregistration.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.preregistration.util.GsonParser;

import java.util.List;

public class PRSubmitModel {
    @Expose
    @SerializedName(value="name")
    private String name ;
    @Expose
    @SerializedName(value="mobile")
    private String mobile ;
    @Expose
    @SerializedName(value="email")
    private String email ;
    @Expose
    @SerializedName(value="stream_id")
    private int streamId ;
    @Expose
    @SerializedName(value="board_id")
    private int boardId ;
    @Expose
    @SerializedName(value="version")
    private int version ;
    @Expose
    @SerializedName(value="application_id")
    private String applicationId ;
    @Expose
    @SerializedName(value="app_version")
    private String appVersion ;
    @Expose
    @SerializedName(value="timestamp")
    private String timestamp ;
    @Expose
    @SerializedName(value="app_name")
    private String appName ;
    @Expose
    @SerializedName(value="board_result_field")
    private List<DynamicInputModel> boardResultField ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getStreamId() {
        return streamId;
    }

    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    public int getBoardId() {
        return boardId;
    }

    public void setBoardId(int boardId) {
        this.boardId = boardId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public List<DynamicInputModel> getBoardResultField() {
        return boardResultField;
    }

    public void setBoardResultField(List<DynamicInputModel> boardResultField) {
        this.boardResultField = boardResultField;
    }

    public String toJson() {
        return GsonParser.toJson(this, new TypeToken<PRSubmitModel>(){});
    }
}
