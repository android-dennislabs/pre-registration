package com.preregistration.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.preregistration.model.entity.PopupEntity;
import com.preregistration.model.entity.StreamEntity;
import com.preregistration.util.GsonParser;

import java.io.Serializable;
import java.util.List;

public class PreRegistrationModel implements Serializable, Cloneable {

    @Expose
    @SerializedName(value="id")
    private Integer id;

    @Expose
    @SerializedName(value="title")
    private String title;

    @Expose
    @SerializedName(value="subTitle", alternate={"sub_title","registration_text"})
    private String subTitle;

    @Expose
    @SerializedName(value="requestApi", alternate={"request_api","post_url"})
    private String requestApi;

    @Expose
    @SerializedName(value="registration_btn_title")
    private String registrationBtnTitle;

    @SerializedName("streams")
    @Expose
    public List<StreamEntity> streams = null;
    @SerializedName("popup")
    @Expose
    public PopupEntity popup;

    @SerializedName("board_result_field")
    @Expose
    private List<DynamicInputModel> inputFields = null;

    @SerializedName("is_registration_form_active")
    @Expose
    private int isRegistrationFormActive ;

    public boolean isRegistrationFormActive() {
        return isRegistrationFormActive == 1;
    }

    public int getIsRegistrationFormActive() {
        return isRegistrationFormActive;
    }

    public void setIsRegistrationFormActive(int isRegistrationFormActive) {
        this.isRegistrationFormActive = isRegistrationFormActive;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getRequestApi() {
        return requestApi;
    }

    public void setRequestApi(String requestApi) {
        this.requestApi = requestApi;
    }

    public List<StreamEntity> getStreams() {
        return streams;
    }

    public void setStreams(List<StreamEntity> streams) {
        this.streams = streams;
    }

    public PopupEntity getPopup() {
        return popup;
    }

    public void setPopup(PopupEntity popup) {
        this.popup = popup;
    }

    public List<DynamicInputModel> getInputFields() {
        return inputFields;
    }

    public void setInputFields(List<DynamicInputModel> inputFields) {
        this.inputFields = inputFields;
    }

    public String getStreamsJson() {
        return GsonParser.toJson(getStreams(), new TypeToken<List<StreamEntity>>() {});
    }

    public String getRegistrationBtnTitle() {
        return registrationBtnTitle;
    }

    public void setRegistrationBtnTitle(String registrationBtnTitle) {
        this.registrationBtnTitle = registrationBtnTitle;
    }

    @NonNull
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public PreRegistrationModel getClone() {
        try {
            return (PreRegistrationModel) clone();
        } catch (CloneNotSupportedException e) {
            return new PreRegistrationModel();
        }
    }
}

