package com.preregistration.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PopupEntity implements Serializable {

    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("submit_btn_text")
    @Expose
    public String submitBtnText;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubmitBtnText() {
        return submitBtnText;
    }

    public void setSubmitBtnText(String submitBtnText) {
        this.submitBtnText = submitBtnText;
    }
}
