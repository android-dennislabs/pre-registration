package com.preregistration.network;

import com.preregistration.model.PRNetworkModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * @author Created by Abhijit Rao on 3/28/2018.
 */

public interface PRApiConfig {
    @POST
    Call<PRNetworkModel> postDataRequest(@Url String url, @QueryMap Map<String, String> options);
}
