package com.preregistration.network;

import android.content.Context;

import com.preregistration.PreRegistration;
import com.preregistration.R;
import com.preregistration.interfaces.PRResponse;
import com.preregistration.model.DynamicInputModel;
import com.preregistration.model.PRNetworkModel;
import com.preregistration.model.PRSubmitModel;
import com.preregistration.util.PRConstant;
import com.preregistration.util.PRUtility;
import com.preregistration.util.TaskRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Callable;

import retrofit2.Response;

public class PRNetworkManager {

    private PRApiConfig mRetrofit;
    private final String host;
    private final Context context;

    public PRNetworkManager(Context context, String host) {
        this.context = context;
        this.host = host;
        if (PRRetrofit.getClient(host) != null) {
            this.mRetrofit = PRRetrofit.getClient(host).create(PRApiConfig.class);
        }
    }

    public void submitRegistration(int boardId, List<DynamicInputModel> mList, String name, String mobile, String email, int streamId, PRResponse.Callback<Boolean> callback) {
        if(mList != null && mList.size() > 0){
            TaskRunner.getInstance().executeAsync(new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    try {
                        String userData = getSubmitModel(boardId, name, mobile, email, streamId, mList).toJson();
                        PRUtility.log("JSON : " + userData);

                        if(mRetrofit != null) {
                             Map<String, String> params = new HashMap<>();
                             params.put("data", PRUtility.encode(userData));
                             Response<PRNetworkModel> response = mRetrofit.postDataRequest(host, params).execute();
                            if (response.body() != null) {
                                return response.body().getStatus().equalsIgnoreCase(PRConstant.SUCCESS);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return false;
                }
            }, new TaskRunner.Callback<Boolean>() {
                @Override
                public void onComplete(Boolean finalData) {
                    callback.onSuccess(finalData);
                }
            });
        }
    }

    private PRSubmitModel getSubmitModel(int boardId, String name, String mobile, String email, int streamId, List<DynamicInputModel> mList){
        PRSubmitModel model = new PRSubmitModel();
        model.setName(name);
        model.setMobile(mobile);
        model.setEmail(email);
        model.setStreamId(streamId);
        model.setBoardId(boardId);
        model.setVersion(PreRegistration.LIBRARY_VERSION);
        model.setApplicationId(context.getPackageName());
        model.setAppVersion(PreRegistration.getInstance().getAppVersion());
        model.setTimestamp(getServerTimeStamp());
        model.setAppName(context.getString(R.string.app_name));
        model.setBoardResultField(mList);
        return model;
    }

    private String getServerTimeStamp() {
        SimpleDateFormat outFmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date date = new Date(System.currentTimeMillis());
        return outFmt.format(date);
    }
}