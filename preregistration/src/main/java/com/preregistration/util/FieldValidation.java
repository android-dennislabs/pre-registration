package com.preregistration.util;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.preregistration.R;

import java.util.regex.Pattern;

/**
 * @author Created by Abhijit on 15-Oct-16.
 */

public class FieldValidation {

    public static boolean isEmpty(Context context, EditText editText, String errorMsg) {
        return hasText(context, editText, errorMsg);
    }

    // call this method when you need to check email validation
    public static boolean isEmail(Context context, EditText editText, boolean required) {
        return isValid(context, editText, EMAIL_REGEX, EMAIL_MSG, required);
    }

    public static boolean isMobileNumber(Context context, EditText editText, boolean required) {
        return isValid(context, editText, MOBILE_REGEX_ADVANCE, MOBILE_MSG, required);
    }


    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String MOBILE_REGEX_ADVANCE = "^[4-9][0-9]{9}$";
    private static final String MOBILE_REGEX = "^[0-9]{10}$|^[0-9]{11}$|^[0-9]{12}$";

    // Error Messages
    private static final String EMAIL_MSG = "Invalid email";
    private static final String MOBILE_MSG = "Invalid mobile";
    private static final String EMPTY = "Empty";

    // return true if the input field is valid, based on the parameter passed
    private static boolean isValid(Context context, EditText editText, String regex, String errMsg, boolean required) {
        try {
            String text = editText.getText().toString().trim();
            // clearing the error, if it was previously set by some other values
            editText.setError(null);
            editText.setOnTouchListener(null);
            // text required and editText is blank, so return false
            if (!hasText(context, editText)) return false;

            // pattern doesn't match so returning false
            if (required && !Pattern.matches(regex, text)) {
                Drawable drawable = ContextCompat.getDrawable(context, R.drawable.pre_ic_fail);
                if(drawable!=null)
                drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
                editText.setError(errMsg, drawable);

                editText.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        final int DRAWABLE_LEFT = 0;
                        final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
                        final int DRAWABLE_BOTTOM = 3;

                        if(event.getAction() == MotionEvent.ACTION_UP) {
                            if(editText.getCompoundDrawables()[DRAWABLE_RIGHT] != null) {
                                if (event.getRawX() >= (editText.getRight() - editText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                                    editText.setError(null);
                                    editText.setText("");
                                    return true;
                                }
                            }
                        }
                        return false;
                    }
                });
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    // check the input field has any text or not
    // return true if it contains text otherwise false
    private static boolean hasText(Context context, EditText editText) {
        return hasText(context, editText, EMPTY);
    }

    private static boolean hasText(Context context, EditText editText, String errorMessage) {

        String text = editText.getText().toString().trim();
        editText.setError(null);
        if (text.length() == 0) {

            SpannableString s = new SpannableString(errorMessage);
            s.setSpan(new ForegroundColorSpan(Color.parseColor("#ffffff")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            s.setSpan(new StyleSpan(Typeface.NORMAL), 0, s.length(), 0);
            s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            Drawable drawable = ContextCompat.getDrawable(context, R.drawable.pre_ic_fail);
            if(drawable!=null)
                drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            editText.setError(s, drawable);
            return false;
        }
        return true;
    }

    public static boolean isValidField(Context context, EditText etMobile, EditText etEmail) {
        if(etMobile.getText().length() == 0 && etEmail.getText().length() == 0){
            PRUtility.showToastCentre(context, context.getString(R.string.error_enter_mobile_or_email));
            return false;
        } else
            return true;
    }
}


