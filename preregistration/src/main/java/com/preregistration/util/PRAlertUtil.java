package com.preregistration.util;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.preregistration.R;
import com.preregistration.model.entity.PopupEntity;

public class PRAlertUtil {

    public static void showSuccessDialog(Activity activity, PopupEntity popup) {
        if(activity != null && popup != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            LayoutInflater inflater = activity.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.pre_alert_success, null);
            builder.setView(dialogView);
            View alertBackground = dialogView.findViewById(R.id.alert_background);
            TextView tvTitle = dialogView.findViewById(R.id.tv_alert_title);
            TextView tvDescription = dialogView.findViewById(R.id.tv_alert_description);
            tvTitle.setText(popup.getTitle());
            tvDescription.setText(popup.getDescription());
            alertBackground.setBackgroundColor(Color.parseColor(PRUtility.getColorValue(activity, "20", R.color.colorPrimary)));
            Button btnAlert = dialogView.findViewById(R.id.btn_alert);
            btnAlert.setText(popup.getSubmitBtnText());
            final AlertDialog dialog = builder.create();
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            btnAlert.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        activity.finish();
                        dialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            dialog.show();
        }
    }
}
