package com.preregistration.util;

public interface PRConstant {

    String NO_DATA = "No Data";

    String SUCCESS = "success";
    String FAILURE = "failure";
    String NO_INTERNET_CONNECTION = "No Internet Connection";

    String CATEGORY_PROPERTY = "category_property";
    String EMAIL_SUGGESTIONS = "[\"@gmail.com\", \"@yahoo.com\", \"@hotmail.com\", \"@outlook.com\"]";

    interface Error {
        String MSG_ERROR = "Error, please try later.";
        String CATEGORY_NOT_FOUND = "Error, This is not supported. Please update";
        String INVALID_FORM_DATA = "Invalid Form Data";
    }
}
