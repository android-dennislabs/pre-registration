package com.preregistration.util;


import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;


public class PRPreferences {

    public static final String IS_REGISTRATION_COMPLETED = "is_registration_completed";

    private static SharedPreferences sharedPreferences;

    public static SharedPreferences getSharedPreferenceObj(Context context){
        if (context != null && sharedPreferences == null )
            sharedPreferences = context.getSharedPreferences(context.getPackageName() , Context.MODE_PRIVATE);

        return sharedPreferences ;
    }

    public static boolean getBoolean(Context context, String key, boolean defaultValue) {
        if (getSharedPreferenceObj(context) != null) {
            return getSharedPreferenceObj(context).getBoolean(key, defaultValue);
        } else {
            return false;
        }
    }
    public static void setBoolean(Context context, String key, boolean value) {
        if (getSharedPreferenceObj(context) != null && !TextUtils.isEmpty(key)) {
            final SharedPreferences.Editor editor = getSharedPreferenceObj(context).edit();
            if (editor != null) {
                editor.putBoolean(encrypt(key), value);
                editor.apply();
            }
        }
    }


    private static String encrypt(String input) {
        return input;
    }

    public static void setRegistrationCompleted(Context context, boolean value) {
        setBoolean(context, IS_REGISTRATION_COMPLETED, value);
    }

    public static boolean isRegistrationCompleted(Context context) {
        return getBoolean(context, IS_REGISTRATION_COMPLETED, false);
    }
}