package com.preregistration.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.preregistration.PreRegistration;

import java.io.UnsupportedEncodingException;

public class PRUtility {

    public static String getColorValue(Context context, String transparentLevel, int colorResource) {
        try {
            String colorValue = Integer.toHexString(ContextCompat.getColor(context, colorResource) & 0x00ffffff);
            if(colorValue.length() < 6){
                switch (colorValue.length()){
                    case 5:
                        colorValue = "0" + colorValue;
                        break;
                    case 4:
                        colorValue = "00" + colorValue;
                        break;
                    case 3:
                        colorValue = "000" + colorValue;
                        break;
                }
            }
            return "#" + transparentLevel + colorValue;
        } catch (Exception e) {
            e.printStackTrace();
            return "#ffffff";
        }
    }

    public static boolean isConnected(Context context) {
        boolean isConnected = false;
        try {
            if (context != null && context.getSystemService(Context.CONNECTIVITY_SERVICE) != null && context.getSystemService(Context.CONNECTIVITY_SERVICE) instanceof ConnectivityManager) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    Network network = connectivityManager.getActiveNetwork();
                    if (network != null) {
                        NetworkCapabilities nc = connectivityManager.getNetworkCapabilities(network);
                        isConnected = nc != null && (nc.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || nc.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || nc.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) || nc.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH));
                    }
                } else {
                    NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
                    isConnected = activeNetwork != null && activeNetwork.isConnected();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnected;
    }

    public static void showPropertyError(Activity activity) {
        if (activity != null) {
            showToastCentre(activity, PRConstant.Error.MSG_ERROR);
            activity.finish();
        }
    }

    public static void showToastCentre(Context context, String msg) {
        if(context != null) {
            Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    public static void log(String message) {
        if(PreRegistration.getInstance().isDebugModeEnabled){
            Log.d(PreRegistration.class.getSimpleName(), message == null ? "null" : message);
        }
    }

    public static String encode(String value) {
        try {
            if(value == null){
                return null;
            }
            byte[] data = value.getBytes("UTF-8");
            return Base64.encodeToString(data, Base64.DEFAULT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return value;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return value;
        } catch (Exception e) {
            e.printStackTrace();
            return value;
        }
    }

    public static void hideKeyboard(Activity activity) {
        try {
            if(activity != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                View f = activity.getCurrentFocus();
                if (null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom(f.getClass()))
                    imm.hideSoftInputFromWindow(f.getWindowToken(), 0);
                else
                    activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
